package Rim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Start {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Для выхода наберите exit");
        while (true) {
            String str0 = reader.readLine();
            try {
                if (str0.equals("exit")) break;
                if (str0.startsWith("0")){
                    System.out.println("Введите цифру больше 0");
                    continue;
                }
                int x = Integer.parseInt(str0);
                String str = new RimV(x).MethodV() + new RimI(x).RimIi();
                str = str.replaceAll("VIIII", "IX");
                str = str.replaceAll("IIII", "IV");
                System.out.println(str);
            }catch (Exception e){
                System.out.println("Введите цифру");
            }
        }


    }
}

